(ns old-british-money.converter-test
  (:require [clojure.test :refer :all]
            [old-british-money.converter :refer :all]))


(deftest pound-to-shilling-test
  (is (= (pound-to-shilling 1) 20))
  )

(deftest shelly-to-penny-test
  (is (= (shilling-to-penny 1) 12))
  )

(deftest pound-to-penny-test
  (is (= (pound-to-penny 1) (* 20 12)))
  )

(deftest price-to-penny-test
  (is (= (price-to-penny 5 17 8) 1412)))

(deftest penny-to-price-test
  (is (= ( penny-to-price 1412) [5 17 8])))

(deftest sum-test
   (is (= (sum [5 17 8] [3 4 10]) [9 2 6])))

(deftest multiply-test
  (is (= (multiply [5 17 8] 2) [11 15 4])))

(deftest subtract-test
  (is (= (subtract [5 17 8] [3 4 10]) [2 12 10])))

(deftest subtract-negative-test
  (is (= (subtract [3 4 10] [5 17 8] ) ["-" 2 12 10])))

(deftest sum-test-2
  (is (= (sum [5 15 8] [4 15 10]) [10 11 6])))

(deftest division-test
  (is (= (divide [5 17 8] 3) [[1 19 2] [0 0 2]] )))

(deftest division-test-2
  (is (= (divide [18 16 1] 15) [[1 5 0] [0 1 1]] )))