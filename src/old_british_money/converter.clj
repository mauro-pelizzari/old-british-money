(ns old-british-money.converter)

;;convert pounds to shilling
(defn pound-to-shilling [p] (* p 20))

;;convert shilling to pennies
(defn shilling-to-penny [s] (* s 12))

;;convert puonds to pennies
(defn pound-to-penny [p] (shilling-to-penny (pound-to-shilling p) ))

;;convert price (p,s,d) to pennies
(defn price-to-penny [p s d]
  (+ (pound-to-penny p) (shilling-to-penny s) d))

;;convert pounds to price
(defn penny-to-price [pennies]
  (let [p (quot pennies (pound-to-penny 1))
        s (quot (mod pennies (pound-to-penny 1)) (shilling-to-penny 1))
        d (mod pennies (shilling-to-penny 1))]
  [p s d]))

;;sum prices
(defn sum [[p1 s1 d1] [p2 s2 d2]]
  (let [add1 (price-to-penny p1 s1 d1)
        add2 (price-to-penny p2 s2 d2)]
    (penny-to-price (+ add1 add2)))
)

;;multiply prices
(defn multiply [[p s d] n]
    (penny-to-price (* (price-to-penny p s d) n))
  )

;;subtract prices
(defn subtract [[p1 s1 d1] [p2 s2 d2]]
  (let [lessening (price-to-penny p1 s1 d1)
        subtracting (price-to-penny p2 s2 d2)]
    (if (> lessening subtracting) (penny-to-price (- lessening subtracting))
                                      (cons "-" (penny-to-price (- subtracting lessening)) ))
    )
  )

;;division prices
(defn divide [[p s d] n]
  [(penny-to-price (quot (price-to-penny p s d) n))
   (penny-to-price (mod (price-to-penny p s d) n))]
  )

